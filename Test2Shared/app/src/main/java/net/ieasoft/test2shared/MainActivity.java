package net.ieasoft.test2shared;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import net.ieasoft.db.ArticlesTable;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context sharedContext = null;
        try {
            sharedContext = this.createPackageContext("net.ieasoft.testshared", Context.CONTEXT_INCLUDE_CODE);
            if (sharedContext == null) {
                return;
            }
        } catch (Exception e) {
            String error = e.getMessage();
            return;
        }
        ArticlesTable a=new ArticlesTable(sharedContext);
        a.Open();
        Cursor c=a.getArticles();
        if(c.moveToFirst())
        {
            Toast.makeText(this, ""+c.getString(1), Toast.LENGTH_SHORT).show();
        }
        a.close();
    }
}
