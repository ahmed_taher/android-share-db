package net.ieasoft.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ahmed on 10/5/2018.
 */

public class ArticlesTable {
    SQLiteDatabase db;
    DatabaseHelper DbHelper;
    final Context context;

    public ArticlesTable(Context c)
    {
        context=c;
        DbHelper=new DatabaseHelper(context);
    }

    static final String KEY_AGENCY_ID="_id";
    static final String KEY_AGENCY_NAME="name";


    static final String DATABASE_TABLE_AGENCY="articles";

    public ArticlesTable Open() throws SQLException
    {
        db = DbHelper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        DbHelper.close();
    }

    public long InsertArticle(long ID,String name)
    {
        ContentValues val=new ContentValues();

        val.put(KEY_AGENCY_ID, ID);
        val.put(KEY_AGENCY_NAME, name);

        return db.insert(DATABASE_TABLE_AGENCY, null, val);
    }

    public boolean deleteAll()
    {
        return db.delete(DATABASE_TABLE_AGENCY, null, null) > 0;
    }

    public Cursor getArticles(){
        return db.query(DATABASE_TABLE_AGENCY, new String[]{KEY_AGENCY_ID,
                KEY_AGENCY_NAME},null, null, null, null, "_id asc");
    }
}
