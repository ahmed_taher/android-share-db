package net.ieasoft.db;

import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by ahmed on 11/3/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper{

    static final String DATABASE_CREATE_Users_Table="create table articles " +
            "(_id integer primary key," +
            "name text not null);";

    static final String TAG="DBAdapter";

    static final String DATABASE_NAME="ArticleDB";

    public static final int DATABASE_VERSION = 4;



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        try {

            db.execSQL(DATABASE_CREATE_Users_Table);


        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS articles");


        onCreate(db);
    }



}