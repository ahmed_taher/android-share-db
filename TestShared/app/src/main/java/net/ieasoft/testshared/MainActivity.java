package net.ieasoft.testshared;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import net.ieasoft.db.ArticlesTable;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArticlesTable a=new ArticlesTable(getApplicationContext());
        a.Open();
        a.InsertArticle(1,"Test");
        Cursor c=a.getArticles();
        if(c.moveToFirst())
        {
            Toast.makeText(this, ""+c.getString(1), Toast.LENGTH_SHORT).show();
        }
        a.close();

    }


}
